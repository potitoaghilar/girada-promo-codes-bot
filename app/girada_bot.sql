-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2017 at 12:21 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `girada_bot`
--

-- --------------------------------------------------------

--
-- Table structure for table `list`
--

CREATE TABLE `list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(512) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `product_price` int(11) NOT NULL,
  `friend_code` varchar(255) DEFAULT NULL,
  `target` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `completed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edited_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `removed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list`
--

INSERT INTO `list` (`id`, `user_id`, `product_name`, `product_code`, `product_price`, `friend_code`, `target`, `created_at`, `completed_at`, `edited_at`, `removed_at`) VALUES
(18, 4, 'ASUS Notebook Zen Book Flip UX360UAK Monitor 13.3\" Full HD Touch Screen Intel Core i5-7200U', 'UX360UAK-C4280T', 32999, NULL, 1, '2017-07-09 21:05:06', '2017-07-29 12:35:32', '0000-00-00 00:00:00', '2017-07-29 23:12:01'),
(21, 4, 'ASUS Notebook Zen Book Flip UX360UAK Monitor 13.3\" Full HD Touch Screen Intel Core i5-7200U', 'UX360UAK-C4280T', 32999, NULL, 0, '2017-07-10 10:57:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-07-29 23:12:01'),
(22, 4, 'SONY PlayStation Plus Card Abbonamento 12 Mesi', 'SNPLUS', 1299, NULL, 2, '2017-07-26 15:43:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-07-29 23:12:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_telegram` int(11) NOT NULL,
  `girada_personal_code` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edited_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_telegram`, `girada_personal_code`, `nome`, `created_at`, `edited_at`, `deleted_at`) VALUES
(4, 132904572, 'POT253EF', 'Potito', '2017-07-09 20:05:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `friend_code` (`friend_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_telegram_2` (`id_telegram`),
  ADD UNIQUE KEY `girada_personal_code_2` (`girada_personal_code`),
  ADD KEY `id_telegram` (`id_telegram`),
  ADD KEY `girada_personal_code` (`girada_personal_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list`
--
ALTER TABLE `list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `list`
--
ALTER TABLE `list`
  ADD CONSTRAINT `list_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `list_ibfk_2` FOREIGN KEY (`friend_code`) REFERENCES `users` (`girada_personal_code`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
