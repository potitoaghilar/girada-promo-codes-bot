from datetime import date, timedelta
from urllib.parse import quote
from bs4 import BeautifulSoup
from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.oxml.shared import OxmlElement, qn
from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml
from docx.shared import Pt
from docx.enum.style import WD_STYLE_TYPE
import os
import comtypes.client
import requests
import json
import time
import MySQLdb
from keyboardOptions import keyboardAvailableOptions
from config import *

### VARS ###
lastListId = None
lastStatsId = None
users = []
# END VARS #


# Application functions
#    Add client to client list
def loginDB():
    global db, dbCursor
    # Database access
    db = MySQLdb.connect(host=dbConfig['host'], user=dbConfig['user'], passwd=dbConfig['passwd'], db=dbConfig['db'])
    db.set_character_set('utf8')
    dbCursor = db.cursor()


def closeDB():
    dbCursor.close()
    db.close()


def start(chat_id, user, dbUser, arguments):
    global users
    found = False
    for currUser in users:
        if currUser['user_id'] == chat_id:
            found = True
            currUser['name'] = arguments[0]
            currUser['user_state'] = '0'
            break
    if found == False:
        users.append({'user_id' : chat_id, 'name' : arguments[0], 'user_state' : '0', 'next_order_product' : None})
    # Start Welcome code
    showWelcome(chat_id, user, dbUser, [])


def showWelcome(chat_id, user, dbUser, arguments):
    # Force user research
    user = findUserById(chat_id)
    if dbUser == None:
        nextInstructions = u'\U0001F4A5' + ' *Entra a far parte della lista!* ' + u'\U0001F4A5' + '\nInviare il comando /inseriscimi per configurare il tuo profilo ed entrare a far parte della lista.\n\nSe vuoi essere spiegato al meglio come funziona il Bot o Girada premi i pulsanti qui di seguito.'
        welcomeKeyboard = 'welcomeNoSubscribed'
        user['user_state'] = '0'
    else:
        nextInstructions = u'\U0001F4A5' + ' *Entra a far parte della lista!* ' + u'\U0001F4A5' + '\nRisulti già registrato nel bot di Telegram.\nDai un\'occhiata a tutto cio che puoi fare pigiando i pulsanti qui in basso!\n\nDai un\'occhiata rapida al tuo codice: /mostra\_codice\_personale'
        welcomeKeyboard = 'welcome'
        user['user_state'] = '2'
    user['next_order_product'] = None
    sendMessage(chat_id, u'\U00002728' + '*BENVENUTO* ' + u'\U00002728' + '\nCiao ' + user['name'] + ', benvenuto su Girada Promo Code List, un Bot Telegram automatizzato per gestire i tuoi codici promozionali per raggiungere il target massimo di sconti su Girada automaticamente!!\n\n%s\n\n' % nextInstructions + u'\U00002139' + ' *Hai bisogno di aiuto?*\nSe hai bisogno di più informazioni non esitare a contattarci!!\nMandaci una mail all\'indirizzo %s' % adminEmail, createKeyboard(keyboardAvailableOptions[welcomeKeyboard]))


def insertNewUser(chat_id, user, dbUser, arguments):
    process = arguments[0]
    if process == 0:
        sendMessage(chat_id, 'Sei già iscritto a Girada?', createKeyboard(keyboardAvailableOptions['yesOrNo']))
        user['user_state'] = '1'
        return
    elif process == 1:
        sendMessage(chat_id, 'Perfetto! Inserisci qui il tuo codice personale per entrare a far parte della community')
        user['user_state'] = '1.setCode'
        return
    elif process == 2:
        sendMessage(chat_id, 'Prima di entrare a far parte della della lista del bot, devi registrarti su Girada all\'indirizzo https://www.girada.it/registrati.html', createKeyboard(keyboardAvailableOptions['home']))
        return
    elif process == 3:
        # Check if user was previously added to list
        dbQuery("SELECT * FROM `users` WHERE `users`.`id_telegram` = '%s' AND `users`.`deleted_at` != '0000-00-00 00:00:00'" % chat_id)
        if dbCursor.rowcount == 0:
            dbQuery("INSERT INTO `users` (`id`, `id_telegram`, `girada_personal_code`, `nome`, `created_at`, `edited_at`, `deleted_at`) VALUES (NULL, '%s', '%s', '%s', CURRENT_TIMESTAMP, '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000');" % (chat_id, arguments[1], user['name']))
        else:
            dbQuery("UPDATE `users` SET `girada_personal_code` = '%s', `nome` = '%s', `deleted_at` = '0000-00-00 00:00:00' WHERE `users`.`id_telegram` = %s" % (arguments[1], user['name'], chat_id))
        user['user_state'] = '1'
        sendMessage(chat_id, 'Benvenuto nella lista automatica di Girada. Leggi il regolamento per capire come fare a risparmiare sui tuoi acquisti.', createKeyboard(keyboardAvailableOptions['home']))
        return
    systemErrorException(chat_id)


def showCode(chat_id, user, dbUser, arguments):
    sendMessage(chat_id, 'Il tuo codice personale Girada è "%s".\n\nSe ritieni necessario dover cambiare il tuo codice riferiscilo all\'amministratore alla seguente mail: %s' % (dbUser[2], adminEmail), createKeyboard(keyboardAvailableOptions['welcome']))


def explainGirada(chat_id, user, dbUser, arguments):
    with open('templates/guida_girada.txt', 'r') as content_file:
        content = content_file.read()
    sendMessage(chat_id, content, createKeyboard(keyboardAvailableOptions['home']))


def explainBot(chat_id, user, dbUser, arguments):
    with open('templates/guida_bot.txt', 'r') as content_file:
        content = content_file.read().replace(':point:', u'\U00002733')
    sendMessage(chat_id, content)


def newOrder(chat_id, user, dbUser, arguments):
    process = arguments[0]
    if process == 0:
        sendMessage(chat_id, 'Vai su Girada e scegli il prodotto da ordinare. Una volta scelto copia ed incolla l\'url da Girada qui per identificare il prodotto.')
        user['user_state'] = '3.setProduct'
        return
    elif process == 1:
        product = fetchProductFromGirada(arguments[1])
        if product == None:
            sendMessage(chat_id, 'Il prodotto non può essere identificato. Assicurati di aver inserito un url simile a questo:\nhttps://girada.it/pages/ecommerce/catalog/detail.asp?uid={XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}&puid={XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}\n\nRiprova:')
        else:
            sendMessage(chat_id, '*Prodotto identificato*\nNome: %s\n\nCodice: %s\n\nPrezzo: %s\n\nConfermi che il prodotto che hai intenzione di ordinare è questo?' % (product['name'], product['code'], product['price']), createKeyboard(keyboardAvailableOptions['yesOrNo']))
            user['user_state'] = '3.prodConfirm'
            user['next_order_product'] = product
        return
    elif process == 2:
        if arguments[1] == '/si':
            # Search user to give reward
            sendMessage(chat_id, 'Conferma l\'ordine per ricevere il codice amico che dovrai inserire in Girada.\n\n*Attenzione!\nL\'operazione non può essere annullata una volta confermata.*', createKeyboard(keyboardAvailableOptions['confirmOrAbortHome']))
            user['user_state'] = '3.confirmOrder'
        elif arguments[1] == '/no':
            sendMessage(chat_id, 'Torna su Girada e scegli il prodotto da ordinare. Una volta scelto copia ed incolla l\'url da Girada qui per identificarlo.')
            user['user_state'] = '3.setProduct'
        return
    elif process == 3:
        if arguments[1] == '/conferma':
            user['user_state'] = '3.confirmed'
            friendUser = getFriendUser(user['next_order_product']['price'], dbUser[0])
            # Text
            if friendUser == None:
                friendCode = "(Non dovrai inserire nessun codice nel tuo caso)"
            else:
                friendCode = friendUser[12]
            sendMessage(chat_id, 'Il codice amico che dovrai inserire per quest\'ordine è il seguente:\n\n*%s*\n\nAssicurati di copiarlo correttamente mantenedo le maiuscole e le minuscole.\n\nPer tornare alla home premi il pulsante /home.' % friendCode, createKeyboard(keyboardAvailableOptions['home']))
            # DB update
            if friendUser == None:
                friendUser = 'NULL'
            else:
                addRewardToUser(friendUser[0])
                friendUser = "\'%s\'" % friendUser[12]
            addOrderInList(dbUser, user['next_order_product'], friendUser)
        else:
            showWelcome(chat_id, user, dbUser, [])
        return
    elif process == 4:
        showWelcome(chat_id, user, dbUser, [])
        return


def payOrder(chat_id, user, dbUser, arguments):
    process = arguments[0]
    if process == 0:
        orders = ""
        getRowsFromDB(dbUser[1])
        for index, row in enumerate(dbCursor):
            orders += str(index) + " - " + str(row[2]) + " (€ " + str(row[4])[:-2] + "," + str(row[4])[-2:] + ") - Amici " + str(row[6]) + "/3\n\n"
        sendMessage(chat_id, '*Gli ordini che puoi pagare anticipatamente sono i seguenti:*\n\n%sInserisci il numero dell\'ordine che vuoi pagare' % orders)
        user['user_state'] = '4.setOrderId'
    elif process == 1:
        getRowsFromDB(dbUser[1])
        for index, row in enumerate(dbCursor):
            if str(index) == arguments[1]:
                setCompleted(row[0])
                sendMessage(chat_id, '*Ordine pagato*\n\n%s' % str(row[2]), createKeyboard(keyboardAvailableOptions['home']))
                user['user_state'] = '4.goHome'
                return
        sendMessage(chat_id, 'Valore inserito errato\n\nInserisci il numero dell\'ordine che vuoi pagare o premi /home per tornare alla home', createKeyboard(keyboardAvailableOptions['home']))


def status(chat_id, user, dbUser, arguments):
    orders = ""
    ordersCompleted = ""
    getRowsFromDB(dbUser[1])
    rowCount = 0
    for index, row in enumerate(dbCursor):
        rowCount += 1
        orders += str(index) + " - " + str(row[2]) + " (€ " + str(row[4])[:-2] + "," + str(row[4])[-2:] + ") - Amici " + str(row[6]) + "/3\n\n"
    if rowCount == 0:
        orders = "(Nessun ordine da mostrare)\n"
    getRowsFromDB(dbUser[1], True)
    rowCount = 0
    for row in dbCursor:
        rowCount += 1
        ordersCompleted += str(row[2]) + " - Amici " + str(row[6]) + "/3\n\n"
    if rowCount == 0:
        ordersCompleted = "(Nessun ordine da mostrare)\n"
    sendMessage(chat_id, '*Ordini effettuati ancora in corso:*\n\n%s\n*Ordini effettuati completati:*\n\n%s\nSe hai intenzione di pagare un\'ordine anticipatamente inserisci /paga\_ordine' % (orders, ordersCompleted), createKeyboard(keyboardAvailableOptions['welcome']))


def showList(chat_id, user, dbUser, arguments):
    global lastListId
    sendMessage(chat_id, 'Solo un momento...')
    if not os.path.isfile('Lista.pdf') or time.time() - os.path.getmtime('Lista.pdf') > 1800:
        createList()
        lastListId = None
    if lastListId == None:
        sendDocument(chat_id, 'Lista.pdf', True, createKeyboard(keyboardAvailableOptions['home']))
    else:
        sendDocument(chat_id, lastListId, False, createKeyboard(keyboardAvailableOptions['home']))


def cancelOrder(chat_id, user, dbUser, arguments):
    process = arguments[0]
    if process == 0:
        orders = ""
        getRowsFromDB(dbUser[1])
        rowCount = 0
        for index, row in enumerate(dbCursor):
            rowCount += 1
            orders += str(index) + " - " + str(row[2]) + " (€ " + str(row[4])[:-2] + "," + str(row[4])[-2:] + ") - Amici " + str(row[6]) + "/3\n\n"
        if rowCount == 0:
            orders = "(Nessun ordine da mostrare)\n"
        sendMessage(chat_id, '*Ordini effettuati ancora in corso:*\n\n%s\nInserisci il numero dell\'ordine che vuoi annullare' % orders, createKeyboard(keyboardAvailableOptions['home']))
        user['user_state'] = '5.deleteOrderId'
    elif process == 1:
        getRowsFromDB(dbUser[1])
        for index, row in enumerate(dbCursor):
            if str(index) == arguments[1]:
                cancelOrderFromDB(row[0])
                sendMessage(chat_id, '*Ordine eliminato*\n\n%s' % str(row[2]), createKeyboard(keyboardAvailableOptions['home']))
                user['user_state'] = '5.goHome'
                return
        sendMessage(chat_id, 'Valore inserito errato\n\nInserisci il numero dell\'ordine che vuoi eliminare o premi /home per tornare alla home', createKeyboard(keyboardAvailableOptions['home']))

# Add-on
#def showPriceList(chat_id, user, dbUser, arguments):
    #sendMessage(chat_id, 'Funzione non ancora disponibile', createKeyboard(keyboardAvailableOptions['home']))
    # TODO

# Add-on
#def received(chat_id, user, dbUser, arguments):
    #sendMessage(chat_id, 'Funzione non ancora disponibile', createKeyboard(keyboardAvailableOptions['home']))
    # TODO


def share(chat_id, user, dbUser, arguments):
    sendMessage(chat_id, '*Il tuo supporto è importate*\n\nIl tuo continuo aiuto a pubblicizzarci ci permette di crescere come comunità. Più utenti entrano a far parte del sistema quanto più facilmente risparmi tu.\n\nCondividi Girada Promo Code List con tutti i tuoi amici attraverso questo link:\n\n%s' % botURL, createKeyboard(keyboardAvailableOptions['home']))


def statistics(chat_id, user, dbUser, arguments):
    global lastStatsId
    sendMessage(chat_id, 'Solo un momento...')
    if not os.path.isfile('Statistiche.pdf') or time.time() - os.path.getmtime('Statistiche.pdf') > 1800:
        createStats()
        lastStatsId = None
    if lastStatsId == None:
        sendDocument(chat_id, 'Statistiche.pdf', True, createKeyboard(keyboardAvailableOptions['home']))
    else:
        sendDocument(chat_id, lastStatsId, False, createKeyboard(keyboardAvailableOptions['home']))


def removeFromList(chat_id, user, dbUser, arguments):
    process = arguments[0]
    if process == 0:
        sendMessage(chat_id, '*ELIMINAZIONE PROFILO*\n\nSicuro di voler essere eliminato in modo permanente dalla lista?\n\n*ATTENZIONE! QUEST\'AZIONE E\' IRREVERSIBILE*\n\nProseguire?', createKeyboard(keyboardAvailableOptions['yesOrNo']))
        user['user_state'] = '6.deleteProfile'
    elif process == 1:
        if arguments[1] == "/si":
            dbQuery("UPDATE `users` SET `deleted_at` = '%s' WHERE `users`.`id` = %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), dbUser[0]))
            dbQuery("UPDATE `list` SET `removed_at` = '%s' WHERE `list`.`user_id` = %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), dbUser[0]))
            sendMessage(chat_id, '*ELIMINAZIONE PROFILO COMPLETATA*\n\nNon fai piu parte della lista', createKeyboard(keyboardAvailableOptions['home']))
        else:
            sendMessage(chat_id, '*ELIMINAZIONE PROFILO ANNULLATA*\n\nIl profilo non è stato cancellato', createKeyboard(keyboardAvailableOptions['home']))
        user['user_state'] = '6.goHome'


def systemErrorException(chat_id):
    sendMessage(chat_id, 'Errore di sistema contattare l\'amministratore del bot all\'indirizzo %s' % adminEmail, createKeyboard(keyboardAvailableOptions['home']))


# Utils functions
def sendMessage(chat_id, message, reply_markup = None):
    request_url = url + '/sendMessage?chat_id=%i&text=%s&parse_mode=Markdown' % (chat_id, quote(message, safe=''))
    if reply_markup:
        request_url += '&reply_markup=%s' % quote(reply_markup, safe='')
    request = requests.get(request_url)
    if request.status_code != 200:
        print(request.content.decode('utf8'))
    return request.status_code == 200


def sendDocument(chat_id, document, isFile, reply_markup = None):
    global lastListId
    if isFile:
        request_url = url + '/sendDocument?chat_id=%i' % chat_id
    else:
        request_url = url + '/sendDocument?chat_id=%i&document=%s' % (chat_id, lastListId)
    if reply_markup:
        request_url += '&reply_markup=%s' % quote(reply_markup, safe='')
    if isFile:
        request = requests.post(request_url, files={'document' : open(document, 'rb')})
        lastListId = toJSON(request.content.decode('utf8'))['result']['document']['file_id']
    else:
        request = requests.get(request_url)
    if request.status_code != 200:
        print(request.content.decode('utf8'))
    return request.status_code == 200


def createKeyboard(options, one_time_keyboard = True):
    keyboard = {"keyboard": [[option] for option in options], 'one_time_keyboard': one_time_keyboard, 'resize_keyboard': True}
    return json.dumps(keyboard)


def findUserById(id):
    global users
    for user in users:
        if user['user_id'] == id:
            return user
    return None

def findUserInDB(chat_id):
    dbQuery("SELECT * FROM `users` WHERE `users`.`deleted_at` = '0000-00-00 00:00:00'")
    for row in dbCursor:
        if row[1] == chat_id:
            return row
    return None


def dbQuery(query):
    dbCursor.execute(query)
    db.commit()


def addOrderInList(dbUser, product, friendCode):
    dbQuery("INSERT INTO `list` (`id`, `user_id`, `product_name`, `product_code`, `product_price`, `friend_code`, `target`, `created_at`, `edited_at`, `removed_at`) VALUES (NULL, '%s', '%s', '%s', '%s', %s, '0', CURRENT_TIMESTAMP, '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000');" % (dbUser[0], product['name'], product['code'], product['price'].replace(",", "").replace("€ ", ""), friendCode))


def addRewardToUser(friendUserOrderId):
    dbQuery("UPDATE `list` SET `target` = `target` + '1' WHERE `list`.`id` = '%s'" % friendUserOrderId)
    dbQuery("UPDATE `list` SET `completed_at` = '%s' WHERE `list`.`target` = '3' AND `list`.`id` = '%s'" % (time.strftime("%Y-%m-%d %H:%M:%S"), friendUserOrderId))


def setCompleted(user_id):
    dbQuery("UPDATE `list` SET `completed_at` = '%s' WHERE `list`.`id` = '%s';" % (time.strftime("%Y-%m-%d %H:%M:%S"), user_id))


def fetchProductFromGirada(url):
    try:
        request = requests.get(url)
    except:
        return None
    if request.status_code == 200:
        data = request.text
        soup = BeautifulSoup(data, "html.parser")
        try:
            prodName = soup.findAll("h2", { "class" : "product-name" })[0].text
            prodCode = soup.find("div", { "id" : "ref_bracketz" }).text[5:]
            prodPrice = soup.findAll("div", { "class" : "detailprice" })[0].text
            return {'name' : prodName, 'code' : prodCode, 'price' : prodPrice}
        except:
            return None
    else:
        return None


def getFriendUser(price, my_user_id):
    # First search by oldest order
    #dbCursor.execute("SELECT * FROM `list` INNER JOIN `users` ON list.`user_id` = users.`id` WHERE DATEDIFF('%s', list.`created_at`) > 30 and list.`target` < 3 LIMIT 1" % time.strftime("%Y-%m-%d %H:%M:%S"))
    #for row in dbCursor:
    #    return row ------------------------ LOGIC ERROR (outdated also)
    # Otherwise search by price
    dbQuery("SELECT * FROM `list` INNER JOIN `users` ON list.`user_id` = users.`id` WHERE list.`product_price` < '%s' AND list.`target` < 3 AND list.`completed_at` = '0000-00-00 00:00:00' AND list.`user_id` != '%s' AND `list`.`removed_at` = '0000-00-00 00:00:00' LIMIT 1" % (price.replace(",", "").replace("€ ", ""), my_user_id))
    for row in dbCursor:
        return row
    return None


def createList():
    document = Document('list-template.docx')
    table = document.tables[0]

    for paragraph in document.paragraphs:
        if "|time|" in paragraph.text:
            paragraph.text = paragraph.text.replace("|time|", time.strftime("%H:%M:%S") + " del " + time.strftime("%d-%m-%Y"))

    getRowsFromDB()
    for row in dbCursor:
        table.add_row()
        newCells = table.rows[len(table.rows) - 1].cells

        # Write content
        newCells[0].text = str(row[7])
        newCells[1].text = str(row[14])
        newCells[2].text = str(row[2])
        newCells[3].text = "€ " + str(row[4])[:-2] + "," + str(row[4])[-2:]
        newCells[4].text = str(row[5]).replace("None", "(Nessuno)")
        newCells[5].text = str(row[6]) + "/3"
        status = str(row[8]).replace("None", "Ordinato. In attesa degli amici...")
        if status != "Ordinato. In attesa degli amici...":
            if str(row[6]) == "3":
                status = "Completato"
            else:
                status = "Completato\n(Pagamento anticipato)"
        newCells[6].text = status

        # Apply alignment
        newCells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        newCells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        newCells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        newCells[3].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        newCells[4].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        newCells[5].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        newCells[6].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        set_cell_vertical_alignment(newCells[0])
        set_cell_vertical_alignment(newCells[1])
        set_cell_vertical_alignment(newCells[2])
        set_cell_vertical_alignment(newCells[3])
        set_cell_vertical_alignment(newCells[4])
        set_cell_vertical_alignment(newCells[5])
        set_cell_vertical_alignment(newCells[6])

        # Apply background color

        gridColor = {0: "ffffff", 1: "a0ffc0", 2: "37d66d", 3: "009e35"}

        if status == "Completato\n(Pagamento anticipato)":
            color = 3
        else:
            color = row[6]

        shading_elm_0 = parse_xml((r'<w:shd {} w:fill="%s"/>' . format(nsdecls('w'))) % gridColor[color])
        shading_elm_1 = parse_xml((r'<w:shd {} w:fill="%s"/>' . format(nsdecls('w'))) % gridColor[color])
        shading_elm_2 = parse_xml((r'<w:shd {} w:fill="%s"/>' . format(nsdecls('w'))) % gridColor[color])
        shading_elm_3 = parse_xml((r'<w:shd {} w:fill="%s"/>' . format(nsdecls('w'))) % gridColor[color])
        shading_elm_4 = parse_xml((r'<w:shd {} w:fill="%s"/>' . format(nsdecls('w'))) % gridColor[color])
        shading_elm_5 = parse_xml((r'<w:shd {} w:fill="%s"/>' . format(nsdecls('w'))) % gridColor[color])
        shading_elm_6 = parse_xml((r'<w:shd {} w:fill="%s"/>' . format(nsdecls('w'))) % gridColor[color])

        newCells[0]._tc.get_or_add_tcPr().append(shading_elm_0)
        newCells[1]._tc.get_or_add_tcPr().append(shading_elm_1)
        newCells[2]._tc.get_or_add_tcPr().append(shading_elm_2)
        newCells[3]._tc.get_or_add_tcPr().append(shading_elm_3)
        newCells[4]._tc.get_or_add_tcPr().append(shading_elm_4)
        newCells[5]._tc.get_or_add_tcPr().append(shading_elm_5)
        newCells[6]._tc.get_or_add_tcPr().append(shading_elm_6)

    document.save('out/listUPDATED.docx')
    docx2pdf('out/listUPDATED.docx', 'out/Lista.pdf')
    os.remove('out/listUPDATED.docx')


def createStats():
    document = Document('statistics-template.docx')

    style = document.styles.add_style('Custom', WD_STYLE_TYPE.PARAGRAPH)
    font = style.font
    font.name = 'Calibri'
    font.size = Pt(14)
    font.bold = True

    dbQuery("SELECT * FROM `list` WHERE  `list`.`removed_at` = '0000-00-00 00:00:00'")
    orders = dbCursor.rowcount

    dbQuery("SELECT * FROM `list` WHERE `list`.`target` = 3")
    completedFriends = dbCursor.rowcount

    dbQuery("SELECT * FROM `list` WHERE `list`.`completed_at` != '0000-00-00 00:00:00' AND `list`.`target` > 0 AND `list`.`removed_at` = '0000-00-00 00:00:00'")
    totEuros = 0
    for row in dbCursor:
        totEuros += row[4]
    if len(str(totEuros)) >= 3:
        totEuros = str(totEuros)[:-2] + ',' + str(totEuros)[-2:]
    else:
        totEuros = '0,' + str(totEuros)

    dbQuery("SELECT * FROM `list` WHERE DATEDIFF('%s', `list`.`created_at`) <= 1 AND `list`.`removed_at` = '0000-00-00 00:00:00'" % time.strftime("%Y-%m-%d %H:%M:%S"))
    new24hOrders = dbCursor.rowcount

    dbQuery("SELECT * FROM `list` WHERE DATE(`list`.`created_at`) = '%s' AND `list`.`removed_at` = '0000-00-00 00:00:00'" % time.strftime("%Y-%m-%d"))
    todayOrders = dbCursor.rowcount

    dbQuery("SELECT * FROM `list` WHERE DATE(`list`.`created_at`) = '%s' AND `list`.`target` = 3" % time.strftime("%Y-%m-%d"))
    completedToday = dbCursor.rowcount

    dbQuery("SELECT * FROM `list` WHERE DATE(`list`.`created_at`) = '%s' AND `list`.`removed_at` = '0000-00-00 00:00:00'" % (date.today() - timedelta(1)).strftime("%Y-%m-%d"))
    yesterdayOrders = dbCursor.rowcount

    dbQuery("SELECT * FROM `list` WHERE DATE(`list`.`created_at`) >= '%s' AND `list`.`removed_at` = '0000-00-00 00:00:00'" % (date.today() - timedelta(7)).strftime("%Y-%m-%d"))
    last7daysOrders = dbCursor.rowcount

    for paragraph in document.paragraphs:
        edited = False
        if "|time|" in paragraph.text:
            paragraph.text = paragraph.text.replace("|time|", time.strftime("%H:%M:%S") + " del " + time.strftime("%d-%m-%Y"))
        if "|orders|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|orders|", str(orders))
        if "|completedFriends|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|completedFriends|", str(completedFriends))
        if "|totEuros|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|totEuros|", str(totEuros))
        if "|new24hOrders|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|new24hOrders|", str(new24hOrders))
        if "|todayOrders|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|todayOrders|", str(todayOrders))
        if "|completedToday|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|completedToday|", str(completedToday))
        if "|yesterdayOrders|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|yesterdayOrders|", str(yesterdayOrders))
        if "|last7daysOrders|" in paragraph.text:
            edited = True
            paragraph.text = paragraph.text.replace("|last7daysOrders|", str(last7daysOrders))
        if edited:
            paragraph.style = document.styles['Custom']

    document.save('out/statisticsUPDATED.docx')
    docx2pdf('out/statisticsUPDATED.docx', 'out/Statistiche.pdf')
    os.remove('out/statisticsUPDATED.docx')


def set_cell_vertical_alignment(cell, align="center"):
    try:
        tc = cell._tc
        tcPr = tc.get_or_add_tcPr()
        tcValign = OxmlElement('w:vAlign')
        tcValign.set(qn('w:val'), align)
        tcPr.append(tcValign)
        return True
    except:
        traceback.print_exc()
        return False


def docx2pdf(in_file, out_file):
    wdFormatPDF = 17

    in_file = os.path.abspath(in_file)
    out_file = os.path.abspath(out_file)

    word = comtypes.client.CreateObject('Word.Application')
    doc = word.Documents.Open(in_file)
    doc.SaveAs(out_file, FileFormat=wdFormatPDF)
    doc.Close()
    word.Quit()


def getRowsFromDB(id_telegram = None, completed_orders = False):
    if id_telegram == None:
        dbQuery("SELECT * FROM `list` INNER JOIN `users` ON list.`user_id` = users.`id` WHERE (`list`.`completed_at` = '0000-00-00 00:00:00' OR DATEDIFF('%s', `list`.`completed_at`) <= 30) AND `list`.`removed_at` = '0000-00-00 00:00:00'" % time.strftime("%Y-%m-%d %H:%M:%S"))
    else:
        if completed_orders == True:
            dbQuery("SELECT * FROM `list` INNER JOIN `users` ON list.`user_id` = users.`id` WHERE `users`.`id_telegram` = '%s' AND DATEDIFF('%s', `list`.`completed_at`) <= 30 AND `list`.`removed_at` = '0000-00-00 00:00:00'" % (id_telegram, time.strftime("%Y-%m-%d %H:%M:%S")))
        else:
            dbQuery("SELECT * FROM `list` INNER JOIN `users` ON list.`user_id` = users.`id` WHERE `users`.`id_telegram` = '%s' AND `list`.`completed_at` = '0000-00-00 00:00:00' AND `list`.`removed_at` = '0000-00-00 00:00:00'" % id_telegram)


def cancelOrderFromDB(order_id):
    dbQuery("UPDATE `list` SET `removed_at` = '%s' WHERE `list`.`id` = %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), order_id))


# General functions (Telegram API functions too)
def getUpdates(currAppOffset):
    request = requests.get(url + '/getUpdates?timeout=%i&offset=%i' % (timeout, currAppOffset))
    return toJSON(request.content.decode('utf8'))


def toJSON(data):
    return json.loads(data)


def checkRequestResult(data):
    return data['ok']


def getLastOffset(data):
    return data['result'][len(data['result']) - 1]['update_id']