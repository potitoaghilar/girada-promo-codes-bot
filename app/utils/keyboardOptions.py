# Keyboard options
keyboardAvailableOptions = {
    'welcomeNoSubscribed' : {'/inseriscimi', '/spiegami_girada', '/spiegami_il_bot'},
    'welcome' : {'/mostra_codice_personale', '/cancella_sottoscrizione', '/spiegami_girada', '/spiegami_il_bot', '/stato', '/lista', '/nuovo_ordine', '/paga_ordine', '/annulla_ordine', '/condividi', '/statistiche'},
    'yesOrNo' : {'/si', '/no'},
    'confirmOrAbortHome' : {'/conferma', '/annulla_e_torna_alla_home'},
    'home' : {'/home'},
}