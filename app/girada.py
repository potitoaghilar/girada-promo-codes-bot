#!/usr/bin/python

# Requires : pip install mysqlclient
#            pip3 install beautifulsoup
#            pip install python-docx
#            pip install comtypes

from app.utils.functions import *

# Offset of row to elaborate
currAppOffset = 0

# Chat ID is same as user ID
def processCommand(command, chat_id, user_name):
    commands = {
        # WelcomeNoSubscribed commands
        '0/start': start,
        '0/inseriscimi': insertNewUser,
        '0/spiegami_girada': explainGirada,
        '0/spiegami_il_bot': explainBot,
        '0/home': showWelcome,

        # Subscription commands
        '1/si': insertNewUser,
        '1/no': insertNewUser,
        '1.setCode' : insertNewUser,
        '1/home' : showWelcome,

        # WelcomeSubscribed commands
        '2/start': start,
        '2/mostra_codice_personale': showCode,
        '2/spiegami_girada': explainGirada,
        '2/spiegami_il_bot': explainBot,
        '2/stato': status,
        '2/lista': showList,
        '2/nuovo_ordine': newOrder,
        '2/paga_ordine': payOrder,
        '2/annulla_ordine': cancelOrder,
        '2/condividi': share,
        '2/statistiche': statistics,
        '2/cancella_sottoscrizione': removeFromList,
        '2/home': showWelcome,

        # NewOrder commands
        '3.setProduct' : newOrder,
        '3.prodConfirm/si' : newOrder,
        '3.prodConfirm/no' : newOrder,
        '3.confirmOrder/conferma' : newOrder,
        '3.confirmOrder/annulla_e_torna_alla_home' : newOrder,
        '3.confirmed/home' : newOrder,

        # PayOrder commands
        '4.setOrderId' : payOrder,
        '4.setOrderId/home' : showWelcome,
        '4.goHome/home' : showWelcome,

        # CancelOrder commands
        '5.deleteOrderId' : cancelOrder,
        '5.deleteOrderId/home' : showWelcome,
        '5.goHome/home' : showWelcome,

        # RemoveFromList commands
        '6.deleteProfile/si' : removeFromList,
        '6.deleteProfile/no' : removeFromList,
        '6.deleteProfile/home' : showWelcome,
        '6.goHome/home' : showWelcome,
    }
    arguments = {
        # WelcomeNoSubscribed params
        '0/start': [user_name],
        '0/inseriscimi': [0],
        '0/spiegami_girada': [],
        '0/spiegami_il_bot': [],
        '0/home' : [],

        # Subscription params
        '1/si': [1],
        '1/no': [2],
        '1.setCode' : [3, command],
        '1/home' : [],

        # WelcomeSubscribed params
        '2/start': [user_name],
        '2/mostra_codice_personale': [],
        '2/spiegami_girada': [],
        '2/spiegami_il_bot': [],
        '2/stato': [],
        '2/lista': [],
        '2/nuovo_ordine': [0],
        '2/paga_ordine': [0],
        '2/annulla_ordine': [0],
        '2/condividi': [],
        '2/statistiche': [],
        '2/cancella_sottoscrizione': [0],
        '2/home' : [],

        # NewOrder params
        '3.setProduct' : [1, command],
        '3.prodConfirm/si' : [2, '/si'],
        '3.prodConfirm/no' : [2, '/no'],
        '3.confirmOrder/conferma' : [3, '/conferma'],
        '3.confirmOrder/annulla_e_torna_alla_home' : [3, '/annulla_e_torna_alla_home'],
        '3.confirmed/home' : [4],

        # PayOrder params
        '4.setOrderId' : [1, command],
        '4.setOrderId/home' : [],
        '4.goHome/home' : [],

        # CancelOrder params
        '5.deleteOrderId' : [1, command],
        '5.deleteOrderId/home' : [],
        '5.goHome/home' : [],

        # RemoveFromList params
        '6.deleteProfile/si' : [1, '/si'],
        '6.deleteProfile/no' : [1, '/no'],
        '6.deleteProfile/home' : [],
        '6.goHome/home' : [],
    }
    # Get user from array and from DB
    user = findUserById(chat_id)
    dbUser = findUserInDB(chat_id)
    # Restart clients after server restart
    if user == None:
        if command != '/start':
            sendMessage(chat_id, 'Il server è stato riavviato dopo l\'ultima esecuzione.\nRipristino bot eseguito.')
        commands['0/start'](chat_id, user, dbUser, [user_name])
        return
    try:
        if command[0:1] == '/':
            commands[user['user_state'] + command](chat_id, user, dbUser, arguments[user['user_state'] + command])
        else:
            commands[user['user_state']](chat_id, user, dbUser, arguments[user['user_state']])
    except:
        sendMessage(chat_id, 'Il comando inserito non è valido. Sceglierne uno differente.')


# Main loop
loginDB()
while True:
    try:
        updates = getUpdates(currAppOffset)
        if checkRequestResult(updates):
            # Execute commands
            for command in updates['result']:
                processCommand(command['message']['text'], command['message']['chat']['id'], command['message']['chat']['first_name'])
            # Update offset
            if len(updates['result']) > 0:
                currAppOffset = getLastOffset(updates) + 1
            else:
                currAppOffset = 0
        else:
            print('A problem with Telegram API was encountered. Bot is not running properly, check configuration please!')
        time.sleep(.5)
    except KeyboardInterrupt:
        print('Python bot interrupted by keyboard...')
        closeDB()
        break
